'use strict'

const tar = require('tar')
const fs = require('fs-extra')
const path = require('path')
const os = require('os')
const debug = require('debug')('pkgs:unpacker')

class DependencyUnpacker {
  constructor (archivePath, dstPath) {
    this.archivePath = archivePath
    this.dstPath = dstPath
    this.defaultTarOpts = {
      file: this.archivePath,
      'strip-components': 1, // remove the leading path element
      keep: true, // don't overwrite existing files
      unlink: true // unlink files before creating them
    }

    this._tar = tar
    this._fs = fs
  }

  async unpack (opts) {
    debug(`extracting files into ${this.dstPath}`)
    const tmpDir = await this._fs.mkdtemp(path.join(os.tmpdir(), path.sep))
    opts = { ...this.defaultTarOpts, ...opts, ...{ cwd: tmpDir } }

    await this._tar.extract(opts)

    const packageDir = path.join(tmpDir, 'package')
    let srcDir
    // remove intermediate directory package if it exists
    try {
      await this._fs.access(packageDir)
      srcDir = packageDir // directory package exists
    } catch (err) {
      if (err.code === 'ENOENT') { // directory package does not exist
        srcDir = tmpDir
      } else { // unexpected error
        throw err
      }
    }

    await this._fs.move(srcDir, this.dstPath)
  }
}

module.exports = DependencyUnpacker
