const fs = require('fs')
const path = require('path')
var grpc = require('grpc')
var protoLoader = require('@grpc/proto-loader')

var PROTO_PATH = path.join(__dirname, '../protos/outputs.proto')
var packageDefinition = protoLoader.loadSync(
  PROTO_PATH,
  { // Suggested options for similarity to existing grpc.load behavior
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
  })

const falco = grpc.loadPackageDefinition(packageDefinition).falco
const OutputService = falco.outputs.service

class FalcoClient {
  constructor () {
    const certsDir = '/etc/falco/certs'
    this.service = new OutputService('localhost:5060', grpc.credentials.createSsl(
      fs.readFileSync(path.join(certsDir, 'server.crt')),
      fs.readFileSync(path.join(certsDir, 'client.key')),
      fs.readFileSync(path.join(certsDir, 'client.crt'))
    ))
  }

  subscribe () {
    // returns an event emitter with events 'data', 'end', 'error', 'status'
    // see also https://www.grpc.io/docs/languages/node/basics/#streaming-rpcs
    return this.service.sub()
  }
}

if (require.main === module) {
  const client = new FalcoClient()
  const stream = client.subscribe()

  stream.on('data', function (alert) {
    console.log(alert)
  })
  stream.on('end', end => {
    console.log('server closed connection')
    client.close()
  })
  stream.on('error', err => {
    console.log(`falco client encountered an error: ${err}`)
  })
  stream.on('status', function (status) {
    console.log(`Status: ${status}`)
  })
}

module.exports = FalcoClient
