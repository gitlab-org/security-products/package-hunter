/* eslint-env mocha */

const sinon = require('sinon')
const assert = require('assert')

const DockerManager = require('../src/docker-manager')

// ToDo: stub out calls to the docker api using sinon
describe('DockerManager Test', function () {
  this.timeout(5000)
  before(async function () {
    await new DockerManager().cleanup()
  })

  beforeEach(function () {
    this.manager = new DockerManager(process.cwd())
  })

  afterEach(async function () {
    await this.manager.cleanup()
  })

  describe('_start', function () {
    it.skip('starts a container', async function () {
      await this.manager._create('fass-0.0.1.tgz')
      await this.manager._start()
    })

    it('fulfils if timeout not reached', async function () {
      sinon.stub(this.manager, '_follow')
      this.manager.container = { id: 111, start: async () => {}, attach: async () => {}, stop: async () => {}, remove: async () => {} }
      await assert.doesNotReject(this.manager._startWithTimeout.bind(this.manager, 99999))
    })

    it('rejects if timeout reached', async function () {
      sinon.stub(this.manager, '_follow')
      let id
      const startFn = () => new Promise(function (resolve, reject) {
        id = setTimeout(() => resolve(), 1000) // simulate start doing work
      })

      this.manager.container = { id: 111, start: startFn, attach: async () => {}, stop: async () => {}, remove: async () => {} }
      await assert.rejects(this.manager._startWithTimeout.bind(this.manager, 1), { name: 'JobTimeoutError', message: `execution of container ${this.manager.container.id} timed out` })
      clearTimeout(id)
    })

    it('validates timeout parameter is not undefined', async function () {
      await assert.rejects(this.manager._startWithTimeout.bind(this.manager, 'foobar'), { name: 'TypeError', message: 'timeout must be an integer greater than 0' })
    })

    it('validates timeout parameter is greater zero', async function () {
      await assert.rejects(this.manager._startWithTimeout.bind(this.manager, -1), { name: 'TypeError', message: 'timeout must be an integer greater than 0' })
    })
  })

  describe.skip('_stop', function () {
    beforeEach(async function () {
      await this.manager._create()
      await this.manager._start()
    })

    it('docker stop', async function () {
      await this.manager._stop()
    })

    it('doesn\'t error if already stopped', async function () {
      await this.manager._stop()
      await this.manager._stop()
    })
  })

  it.skip('docker cp', async function () {
    await this.manager._create('fass-0.0.1.tgz')
    await this.manager._cpIntoContainer(process.cwd() + '/fass-0.0.1.tgz', '/tmp')
  })

  it.skip('docker run', async function () {
    await this.manager._create('csc-0.37.1.tgz')
  })

  it.skip('docker exec', async function () {
    await this.manager._create()
    await this.manager._start()
    await this.manager._exec('csc-0.37.1.tgz')
  })

  describe('cleanup', function () {
    it('stops and removes container', async function () {
      const stopSpy = sinon.spy()
      const removeSpy = sinon.spy()

      this.manager.container = {
        stop: stopSpy,
        remove: removeSpy
      }
      await this.manager.cleanup()

      assert(stopSpy.calledOnce)
      assert(removeSpy.calledOnce)
    })

    it('does not error if nothing to cleanup', async function () {
      this.manager._stop = async function () {
        const error = new Error('no such container')
        error.reason = 'no such container'
        return error
      }
      await this.manager.cleanup()
    })
  })
})
